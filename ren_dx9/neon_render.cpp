#include "stdafx.h"
#include "i_renderer.h"
#include "render_r1.h"
#include "neon_render.h"


NeonGraphics *NeonGraphics::p_instance = 0;

NeonGraphics::NeonGraphics():	m_render_r1(new Render_R1)
{	}

NeonGraphics::~NeonGraphics()
{
	delete m_render_r1;
//	delete m_render_r2;
//	delete m_render_r3;
//	delete m_render_r4;
}




RENDERER_API NeonGraphics &NeonGraphics::GetInstance()
{
	if (p_instance == nullptr) 	p_instance = new NeonGraphics();
	return *p_instance;
}

void RENDERER_API NeonGraphics::Init(NeonGraphics::Renderers renderer)
{
	enum_to_obj(renderer)->Initialize();
}

void RENDERER_API NeonGraphics::Run(NeonGraphics::Renderers renderer)
{
	enum_to_obj(renderer)->Run();
}

void RENDERER_API NeonGraphics::Release(NeonGraphics::Renderers renderer)
{
	enum_to_obj(renderer)->Release();
}


IRenderer *NeonGraphics::enum_to_obj(NeonGraphics::Renderers &renderer)
{
	switch (renderer)
	{
	case Renderers::Render_DX9:
		return m_render_r1;
	/*case Renderers::Render_DX10:
	return m_render_r2;
	case Renderers::Render_DX11:
	return m_render_r3;
	case Renderers::Render_DX12:
	return m_render_r4;*/
	}
}