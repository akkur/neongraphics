#pragma once



class IRenderer
{
public:
	IRenderer();
	virtual ~IRenderer();

	virtual void Initialize() = 0;
	virtual void Run() = 0;
	virtual void Release() = 0;
};