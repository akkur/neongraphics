#pragma once

class IRenderer;


class Render_R1:	public IRenderer
{
public:
	Render_R1();
	virtual ~Render_R1();

	virtual void Initialize() override;
	virtual void Run() override;
	virtual void Release() override;
};