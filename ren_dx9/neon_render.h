#pragma once
#include "ren_dllexp.h"

class IRenderer;
class Render_R1;


class NeonGraphics
{
public:
	enum class Renderers
	{
		Render_DX9,
		Render_DX10,
		Render_DX11
//		Render_DX12
	};

	RENDERER_API static NeonGraphics& GetInstance();

	virtual void RENDERER_API Init(NeonGraphics::Renderers renderer);
	virtual void RENDERER_API Run(NeonGraphics::Renderers renderer);
	virtual void RENDERER_API Release(NeonGraphics::Renderers renderer);

private:
	RENDERER_API NeonGraphics();
	virtual RENDERER_API ~NeonGraphics();

	NeonGraphics(const NeonGraphics&) = delete;
	NeonGraphics& operator=(const NeonGraphics&) = delete;

	static NeonGraphics *p_instance;
	IRenderer *m_render_r1;
//	IRenderer *m_render_r2;
//	IRenderer *m_render_r3;
//	IRenderer *m_render_r4;

	IRenderer *enum_to_obj(NeonGraphics::Renderers &renderer);
};
