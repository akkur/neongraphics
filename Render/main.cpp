#include "stdafx.h"
#include "test_window.h"
#include "../ren_dx9/neon_render.h"

#pragma comment(lib, "ren_dx9.lib")
//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")



int main()
{
	HICON hIcon = LoadIcon(GetModuleHandle(0), MAKEINTRESOURCE(IDI_ICON1));
	SendMessage(GetConsoleWindow(), WM_SETICON, 1, (LPARAM)hIcon);
	SetConsoleTitle(L"render_debug");
	SetWindowPos(GetConsoleWindow(), HWND_BOTTOM, 5, 5, 550, 400, SWP_NOZORDER);

	Window wnd;
	wnd.Create(L"NeonRender", 700, 500);

	NeonGraphics &graphics = NeonGraphics::GetInstance();
	graphics.Init(NeonGraphics::Renderers::Render_DX9);
	

	MSG msg;
	while (1)
	{
		Sleep(0);
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			graphics.Run(NeonGraphics::Renderers::Render_DX9);
		}
	}
	graphics.Release(NeonGraphics::Renderers::Render_DX9);
	return(msg.wParam);
}






/*MSG msg;
while (GetMessage(&msg, NULL, 0, 0))
{
Sleep(0);
TranslateMessage(&msg);
DispatchMessage(&msg);
}
return msg.wParam;*/