#include "stdafx.h"
#include "test_window.h"


Window::Window():	m_hinst(0),	m_hwnd(0),	m_classname(L"render_app"), m_last_height(0), m_last_width(0)
{	}

Window::~Window()
{	}

void Window::Create(const wchar_t * title, int width, int height)
{
	m_last_width = width;
	m_last_height = height;
	
	m_wc.style = 0;
	m_wc.lpfnWndProc = procedure;
	m_wc.cbClsExtra = m_wc.cbWndExtra = 0;
	m_wc.hInstance = m_hinst;
	m_wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	m_wc.hIcon = NULL;
	m_wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 3);
	m_wc.lpszMenuName = NULL;
	m_wc.lpszClassName = m_classname;

	if (!RegisterClass(&m_wc)) PostQuitMessage(0);

	m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_classname, L"", WS_OVERLAPPEDWINDOW, (ScreenWidth / 2) - (width / 2), (ScreenHeight / 2) - (height / 2), width, height, NULL, NULL, m_hinst, (LPVOID)this);
	SetWindowText(m_hwnd, title);

	HICON hIcon = LoadIcon(GetModuleHandleW(0), MAKEINTRESOURCE(IDI_ICON1));
	SendMessage(m_hwnd, WM_SETICON, 1, (LPARAM)hIcon);

	ShowWindow(m_hwnd, SW_SHOW);
	UpdateWindow(m_hwnd);
	SetFocus(m_hwnd);
}

void Window::Destroy()
{	
	DestroyWindow(this->m_hwnd);
	UnregisterClass(m_classname, GetModuleHandleW(L"ren_dx9.dll"));
}

void Window::set_fullscreen(bool fscreen)
{
	switch (fscreen)
	{
	case true:
		SetWindowLong(m_hwnd, GWL_STYLE, ~WS_OVERLAPPEDWINDOW & GetWindowLong(m_hwnd, GWL_STYLE));
		SetWindowLong(m_hwnd, GWL_STYLE, GetWindowLong(m_hwnd, GWL_STYLE) | WS_POPUP);
		SetWindowPos(m_hwnd, HWND_BOTTOM, 0, 0, ScreenWidth, ScreenHeight, SWP_NOZORDER);
		break;
	case false:
		SetWindowLong(m_hwnd, GWL_STYLE, ~WS_POPUP & GetWindowLong(m_hwnd, GWL_STYLE));
		SetWindowLong(m_hwnd, GWL_STYLE, GetWindowLong(m_hwnd, GWL_STYLE) | WS_OVERLAPPEDWINDOW);
		SetWindowPos(
			m_hwnd, 
			HWND_BOTTOM, 
			(ScreenWidth / 2) - (m_last_width / 2), 
			(ScreenHeight / 2) - (m_last_height / 2), 
			m_last_width, 
			m_last_height, 
			SWP_NOZORDER);
		break;
	}
}



long Window::procedure(HWND hwnd, unsigned int msg, unsigned int wp, long lp)
{
	if (msg == WM_DESTROY)
	{
		Window *pWnd = (Window*)(::GetWindowLongPtr(hwnd, GWLP_USERDATA));
	}
	if (msg == WM_NCCREATE)
	{
		Window *pWnd = (Window*)(((LPCREATESTRUCT(lp))->lpCreateParams));
		::SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)(pWnd));
		return true;
	}
	else
	{
		Window *pWnd = (Window*)(::GetWindowLongPtr(hwnd, GWLP_USERDATA));
		if (!pWnd) return false;
		return pWnd->WinProc(hwnd, msg, wp, lp);
	}
}

long Window::WinProc(HWND hwnd, unsigned int msg, unsigned int wp, long lp)
{
	switch (msg)
	{
	case WM_SHOWWINDOW:		return onShow(hwnd, msg, wp, lp);
	case WM_PAINT:			return onPaint(hwnd, msg, wp, lp);
	case WM_DESTROY:		return onDestroy(hwnd, msg, wp, lp);
	case WM_KEYDOWN:		return onKeyDown(hwnd, msg, wp, lp);
	default:
		return			DefWindowProc(hwnd, msg, wp, lp);
	}
}

long Window::onShow(HWND hwnd, unsigned int msg, unsigned int wp, long lp)
{
	return 0;
}

long Window::onPaint(HWND hwnd, unsigned int msg, unsigned int wp, long lp)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hwnd, &ps);
	EndPaint(hwnd, &ps);
	return 0;
}

long Window::onDestroy(HWND hwnd, unsigned int msg, unsigned int wp, long lp)
{
	UnregisterClass(m_classname, GetModuleHandleW(L"ren_dx9.dll"));
	PostQuitMessage(0);
	return 0;
}

long Window::onKeyDown(HWND hwnd, unsigned int msg, unsigned int wp, long lp)
{
	switch (wp)
	{
	case VK_ESCAPE:
		PostQuitMessage(0);
		break;
	case VK_CONTROL:
		set_fullscreen(true);
		break;
	case VK_SHIFT:
		set_fullscreen(false);
		break;
	}
	return 0;
}





