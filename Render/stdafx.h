#pragma once

#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include "resource.h"

#define ScreenWidth GetSystemMetrics(SM_CXSCREEN)
#define ScreenHeight GetSystemMetrics(SM_CYSCREEN)