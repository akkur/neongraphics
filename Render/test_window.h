#pragma once

struct HWND__;
typedef HWND__ *HWND;
struct HINSTANCE__;
typedef HINSTANCE__ *HINSTANCE;
struct tagWNDCLASSW;
typedef tagWNDCLASSW WNDCLASS;




class Window
{
public:
	Window();
	virtual ~Window();

	void Create(const wchar_t *title, int width, int height);
	void Destroy();

private:
	WNDCLASS		m_wc;
	HWND			m_hwnd;
	HINSTANCE		m_hinst;
	const wchar_t	*m_classname;

	int				m_last_width;
	int				m_last_height;

	void	set_fullscreen(bool fscreen);

protected:
	static long __stdcall	procedure(HWND hwnd, unsigned int msg, unsigned int wp, long lp);

	long WinProc(HWND hwnd, unsigned int msg, unsigned int wp, long lp);
	long onShow(HWND hwnd, unsigned int msg, unsigned int wp, long lp);
	long onPaint(HWND hwnd, unsigned int msg, unsigned int wp, long lp);
	long onDestroy(HWND hwnd, unsigned int msg, unsigned int wp, long lp);
	long onKeyDown(HWND hwnd, unsigned int msg, unsigned int wp, long lp);
};

